'use strict'

const config = require('../nuxt.config.js')
const siteConf = require('../config')

const express = require('express')
const consola = require('consola')

const Database = require('./database')

const http = require('http')

const SocketIO = require('socket.io')
const Socket = require('./socket')

const { Nuxt, Builder } = require('nuxt')

config.dev = !(process.env.NODE_ENV === 'production')

class Ma5rooba {
  constructor() {
    this.host = process.env.HOST || '127.0.0.1'
    this.port = process.env.PORT || 3000

    // Initiate electron app
    this.app = express()
    this.app.set('port', this.port)

    // Initiate Database
    this.database = new Database(siteConf.database)

    // Initiat Socket
    this.server = new http.Server(this.app)
    this.socket = new Socket(
      SocketIO(this.server, {
        transports: ['websocket']
      })
    )

    // Initiate Nuxt
    this.nuxt = new Nuxt(config)

    this.start()
  }

  async start() {
    // Build only in dev mode
    if (config.dev) {
      const builder = new Builder(this.nuxt)
      await builder.build()
    }

    // Give nuxt middleware to express
    this.app.use(this.nuxt.render)
    this.server.listen(this.port, this.host)

    consola.ready({
      message: `Server listening on http://${this.host}:${this.port}`,
      badge: true
    })
  }
}

new Ma5rooba()
